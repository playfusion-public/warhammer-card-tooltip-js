import Popper from "popper.js"

const CARD_API_URL = "https://carddatabase.warhammerchampions.com/warhammer-cards/_search"
const CARD_IMAGES_URL = "https://assets.warhammerchampions.com/card-database/cards"
const CARD_DATABASE_URL = "https://www.warhammerchampions.com/decks-and-packs/card-database"
const CARD_DATABASE_CARD_PATH = "/card"
const DEFAULT_CARD_CLASS_NAME = "warhammer-card-name"
const DEFAULT_CARD_ACTIVATED_CLASS_NAME = "warhammer-card-name-active"
const DEFAULT_TOOLTIP_CLASS_NAME = "warhammer-card-tooltip"
const DEFAULT_TOOLTIP_VISIBLE_CLASS_NAME = "warhammer-card-tooltip-visible"

const DEFAULT_CSS = `
.${DEFAULT_CARD_ACTIVATED_CLASS_NAME} {
  text-decoration: underline;
  text-decoration-style: double;
  cursor: help;
}

.${DEFAULT_TOOLTIP_CLASS_NAME} {
  max-width: 80vw;
  width: 300px;
  pointer-events: none;
}

.${DEFAULT_TOOLTIP_CLASS_NAME} img {
  position: relative;
  top: 3px;
  width: 100%;
  height: auto;
  opacity: 0;
  border-radius: 8px;
  transform: scale(0);
  transform-origin: center top;
  transition: opacity 400ms, transform 400ms;
}

.${DEFAULT_TOOLTIP_VISIBLE_CLASS_NAME} img {
  opacity: 1;
  transform: scale(1);
}
`

function fetchCardSkus (cardFields, cardNames) {
  return fetch(CARD_API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      size: cardNames.length,
      _source: ["name", "skus"].concat(cardFields),
      query: {
        bool: {
          should: cardNames.map(n => ({term: {"name.normalized": n}}))
        }
      }
    })
  }).then(resp => resp.json()).then(data => {
    return data.hits.hits.reduce((acc, hit) => {
      acc[hit._source.name.toLowerCase()] = hit._source
      return acc
    }, {})
  })
}

function addStyle (css) {
  const style = document.createElement("style")
  style.innerHTML = css
  document.body.appendChild(style)
}

function getDefaultSkuId (cardData, lang) {
  return cardData.skus.filter(sku => sku.default && sku.lang === lang)[0].id
}

function unique (array) {
  return array.filter((c, i, a) => a.indexOf(c) === i)
}

function createTooltip (cardLink, cardData, cardImagesBaseUrl, {language}) {
  const skuId = getDefaultSkuId(cardData, language)
  const div = document.createElement("div")
  const img = document.createElement("img")
  img.src = CARD_IMAGES_URL + "/" + skuId + ".jpg"
  div.className = DEFAULT_TOOLTIP_CLASS_NAME
  div.appendChild(img)
  return div
}

let lastClicked

function attachTooltip (element, tooltip, options) {
  element.addEventListener("mouseover", ev => options.onMouseOver(ev, tooltip))
  element.addEventListener("mouseout", ev => {
    lastClicked = null
    options.onMouseOut(ev, tooltip)
  })
  document.body.appendChild(tooltip)
  new Popper(element, tooltip, options.popperOptions)
}

const PROCESSED_LINKS = new Set()
const isProcessed = Set.prototype.has.bind(PROCESSED_LINKS)
const markProcessed = Set.prototype.add.bind(PROCESSED_LINKS)

function activateCardLink ({element}, cardData, cardDatabaseUrl, options) {
  element.classList.add(DEFAULT_CARD_ACTIVATED_CLASS_NAME)
  if (options.linkToCardDatabase) {
    const a = document.createElement("a")
    a.href = cardDatabaseUrl
    a.addEventListener("click", ev => {
      if (lastClicked !== ev.target) {
        // First tap activates the tooltip, the second tap clicks the link
        lastClicked = ev.target
        ev.preventDefault()
      }
    })
    while (element.firstChild) {
      a.appendChild(element.firstChild)
    }
    element.appendChild(a)
  }
}

function generateCardUrl (cardName, language) {
  return `${CARD_DATABASE_URL}/${language}/${CARD_DATABASE_CARD_PATH}/${cardName}/`
}

const defaultOptions = {
  findCardLinks: () => document.getElementsByClassName(DEFAULT_CARD_CLASS_NAME),
  getCardName: (element) => element.dataset.cardName || element.innerText,
  activateCardLink,
  createTooltip,
  popperOptions: {},
  language: "en",
  linkToCardDatabase: false,
  injectDefaultStyle: true,
  cardFields: [],
  onMouseOver: (ev, tooltip) => tooltip.classList.add(DEFAULT_TOOLTIP_VISIBLE_CLASS_NAME),
  onMouseOut: (ev, tooltip) => tooltip.classList.remove(DEFAULT_TOOLTIP_VISIBLE_CLASS_NAME)
}

export function init (options = {}) {
  options = {
    ...defaultOptions,
    // Default value for injectDefaultStyle is dependent on whether createTooltip has been overridden
    // It can still be explicitly set to true or false.
    injectDefaultStyle: !options.createTooltip,
    ...options
  }
  const elements = Array.from(options.findCardLinks()).filter(e => !isProcessed(e))
  const cardLinks = elements.map((element) => ({element, name: options.getCardName(element)}))
  const uniqueLowerCaseNames = unique(cardLinks.map(c => c.name.toLowerCase()))
  // If there are no names found, then we can stop here
  if (uniqueLowerCaseNames.length === 0) return
  fetchCardSkus(options.cardFields, uniqueLowerCaseNames).then(cardsData => {
    // Add default styles if required
    if (options.injectDefaultStyle) {
      addStyle(DEFAULT_CSS)
    }
    cardLinks.forEach(cardLink => {
      // Check again as this may have changed while the request was made
      if (isProcessed(cardLink.element)) return
      // Check we actually found a record for this card
      const cardData = cardsData[cardLink.name.toLowerCase()]
      if (!cardData) {
        return console.error("No card data found for", cardLink.name)
      }
      // Create a tooltip
      const tooltip = options.createTooltip(cardLink, cardData, CARD_IMAGES_URL, options)
      // Add it to the DOM and call PopperJS to attach it
      if (tooltip) attachTooltip(cardLink.element, tooltip, options)
      // Update the original card name element to highlight it is interesting
      options.activateCardLink(cardLink, cardData, generateCardUrl(cardData.name, options.language), options)
      markProcessed(cardLink.element)
    })
  }, (e) => console.log("Error retrieving card data", e))
}