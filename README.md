# Warhammer Age of Sigmar: Champions Card Tooltips

## Require

You can include the JavaScript file in your page and it will export a global variable `warhammerCardTooltip`

```html
<script src="warhammer-card-tooltip.min.js"></script>
<script>
    warhammerCardTooltip.init()
</script>
```

Alternatively the module is exposed as a UMD module so can be imported as an AMD or CommonJS module.

```javascript
// AMD
define(["./warhammer-card-tooltip.min.js"], (tooltip) => {
  ...
})
// CommonJS
const tooltip = require("./warhammer-card-tooltip.min.js")
```

## Simple usage

```html
<p>Check out this amazing card: <span class="warhammer-card-name">Aetherwing Scout</span></p>
<p>Also have you seen <span class="warhammer-card-name" data-card-name="Abjuration">this one</span>?</p>
<script>
    warhammerCardTooltip.init()
</script>
```        

This will highlight these elements with an underline,
and when the user hovers over the text or taps it on mobile
they will be presented with a tooltip of the card graphic.

## Simple Styling

If you wish to override the style of the links you can add styles to the following classes:

* warhammer-card-name - the default class name for card links (added by you, not the library)
* warhammer-card-name-active - added to a link once activated
* warhammer-card-tooltip - the tooltip container element
* warhammer-card-tooltip-visible - added to the tooltip container element while the card name is hovered

You can choose to override parts of these styles e.g. using rules with higher specificity or !important flags,
or you can set `injectDefaultStyle` to false and create your own from scratch using these classes.

If you need even more flexibility use the advanced options below to create your own custom tooltips.

## Advanced Options

Options are passed in a JS object {} as the only parameter to the init function.

See `example/index.html` for loads of different examples of using these options.

#### findCardLinks

Function which will be called to find all card links to process on the page.

* Receives no arguments
* Return a NodeList (or something else that Array.from() can handle).

Example:
```javascript
warhammerCardTooltip.init({
    findCardLinks: function () {
        return document.querySelectorAll("#intro .my-card-links")
    }
})
```

#### getCardName

Function which will be called to determine the name of the card from the link element.

* Receives
  * element - The card link element
* Return a string card name to look up, e.g. "Abjuration"

Example:
```javascript
warhammerCardTooltip.init({
    getCardName: function (element) {
        return element.dataest.myCardNameDataAttribute
    }
})
```

#### activateCardLink

Function which will be called when the card name has been successfully looked up and so a tooltip has been added to this link.

* Receives
  * cardLink - An object containing `{element: The card link element, name: The card name}`

Example:
```javascript
warhammerCardTooltip.init({
    activateCardLink: function (cardLink, cardDatabaseUrl) {
        cardLink.element.classList.add("super-cool-link")
        if (options.linkToCardDatabase) {
            cardLink.element.innerHTML += ` <a href="${cardDatabaseUrl}">(card db)</a>`
        }
    }
})
```

#### createTooltip

Function which creates the dom element for the tooltip.
Overriding this allows you to create more complicated tooltips including more than just the card image,
or tooltips with custom markup.

* Receives
  * cardLink - An object containing `{element: The card link element, name: The card name}`
  * cardData - The card data from the card API, by default containing {name, skus} but more information
  can be included by overriding the cardFields option.
  * cardImagesBaseUrl - For convenience the base url for card images is provided. Card images urls are cardImageUrl + sku.id + ".jpg".
  For more information on this please see https://www.warhammerchampions.com/decks-and-packs/card-data-api-access/
  * options - A copy of the options passed in to the init function, with defaults added.
* Return a dom node

Example:
```javascript
warhammerCardTooltip.init({
    createTooltip: function (cardLink, cardData, cardImagesBaseUrl, options) {
        const div = document.createElement("div")
        const sku = cardData.skus[0]
        div.innerHTML = `<div class="my-inner-div"><h3>${cardData.name}</h3><img src="${cardImagesBaseUrl}/${sku.id}.jpg" /></div>`
        return document.querySelectorAll("#intro .my-card-links")
    }
})
```

Note: the element returned will be passed to PopperJs as the tooltip parent, which will apply some styles including transform,
so you may need to use another element within that to have full control of style, e.g. for a transform.

#### cardFields ([])

If you are using your own `createTooltip` implementation you might want more information than just card name and sku
returned from the API so you can override this to choose which fields to return. Alternately set it to "*" to return
the whole card record. You do not need to specify `name` or `skus` as these are always requested.

#### popperOptions ({})

You can override the options passed to PopperJs - see [their documentation](https://popper.js.org/) for more information. 

#### language ("en")

If you are using the default implementation of `createTooltip` this will be used to decide which language image to show.

At present possible values are "en" and "de".

#### linkToCardDatabase (false)

If true this will wrap the inner content of the card link element with a link to the Warhammer Age of Sigmar: Champions card database.

#### injectDefaultStyle (true unless createTooltip is overridden, then false)

If you are using the default implementation of `createTooltip` then by default this will inject some sensible styles
to show card images in those popups. If you prefer to keep the generated markup but style them yourself then set this 
to false and provide styles through your own stylesheet.

#### onMouseOver
#### onMouseOver

These functions are triggered when the user mouses over/out of the card name

* Receives
  * ev - A standard dom event, use ev.target to get the card name element
  * tooltip - The tooltip element

## Thanks

Big thanks to @CronikCRS for inspiring this library, and to all the awesome content creators in the
Warhammer Age of Sigmar: Champions community for making fun things, and making things fun!

Come chat with us on [Discord](https://discord.gg/J59SZNN)!

## Bugs, Feedback and Contributing

If you spot bugs, have feedback or want to contribute you can:
* Fork the project, fix the bug or add the feature you think would be amazing, create a pull request
* Open an issue on the Gitlab repository
* Come chat on our [official Discord server](https://discord.gg/J59SZNN), in #playfusion-feedback or #dev-content-creators
* Sob uncontrollably 😭