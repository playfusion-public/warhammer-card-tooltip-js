process.on("unhandledRejection", err => {
  console.error(err.message)
  console.trace()
  throw err
})

const S3 = require("aws-sdk/clients/s3")
const fs = require("fs")
const appRoot = require("app-root-path")
const program = require("commander")

const s3 = new S3({apiVersion: "2006-03-01"})

async function uploadFileToS3 (bucket, key, file, type) {
  console.log("Uploading", file, "to", key)
  await s3.putObject({
    Bucket: bucket,
    Key: key,
    Body: fs.readFileSync(file),
    ACL: "public-read",
    ContentType: type,
  }).promise()
}

async function uploadJsFileToS3 (bucket, key, file) {
  return await uploadFileToS3(bucket, key, file, "application/javascript")
}

async function pathExists (bucket, path) {
  const list = await s3.listObjects({Bucket: bucket, Prefix: path}).promise()
  return list.Contents.length > 0
}

function deslashUrl (url) {
  // Removes double slashes within a url, except after the protocol
  return url.replace(/(?<![a-z]+:)\/\//g, "/")
}

async function publish ({s3Bucket, s3Path, localPath, force, appVersion}) {
  const version = appVersion || require(appRoot + "/package.json").version
  console.log(
    "Preparing to deploy version",
    version,
    "from",
    localPath,
    "to",
    "s3://" + s3Bucket + "/" + s3Path
  )

  const versionS3Path = s3Path + "/" + version
  const exists = await pathExists(s3Bucket, versionS3Path)

  if (exists) {
    console.log("Version " + version + " already exists in S3.")
    if (force) {
      console.log("Force flag is set, overwriting.")
    } else {
      console.log(
        "If you wish to overwrite it please specify the --force option and run the script again."
      )
      process.exit(1)
    }
  }

  localPath = localPath.replace(/\/$/, "")
  let files
  try {
    files = fs.readdirSync(localPath).filter(f => f.endsWith(".js"))
  } catch (e) {
    console.log("Couldn't read script path dir.")
    console.log(e.message)
    process.exit(1)
  }

  if (files.length === 0) {
    console.log("No js files found")
    process.exit(1)
  }

  const uploads = files.map(filename =>
    uploadJsFileToS3(
      s3Bucket,
      `${versionS3Path}/${filename}`,
      `${localPath}/${filename}`
    )
  )
  await Promise.all(uploads)

  console.log("Deployment complete")
}

program
  .version("0.1.0")
  .option("--s3-bucket <bucket-name>", "S3 bucket to upload to")
  .option(
    "--s3-path <path>",
    "Path on the S3 bucket to upload to (omit for the root)"
  )
  .option(
    "--local-path <file-path>",
    "Path of local file to upload"
  )
  .option(
    "--app-version [app-version]",
    "Override app version, typically read from project/package.json if not set"
  )
  .option(
    "--force [app-version]",
    "Override app version, typically read from project/package.json if not set"
  )
  .parse(process.argv)

publish(program)